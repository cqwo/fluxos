/*
 *
 *  * Copyright (C) 2017.
 *  * 用于JAVA项目开发
 *  * 重庆英卡电子有限公司 版权所有
 *  * Copyright (C)  2017.  CqingWo Systems Incorporated. All rights reserved.
 *
 */

package com.cqwo.fluxos.web.framework.controller;

import com.cqwo.fluxos.core.helper.WebHelper;
import com.cqwo.fluxos.web.framework.workcontext.WebWorkContext;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.io.IOException;

@Controller
public class BaseWebController extends BaseController {


    protected WebWorkContext workContext;


    @Override
    protected WebWorkContext getWorkContext() {
        return workContext;
    }

    @ModelAttribute
    public void setInitialize(ServerHttpResponse response, ServerHttpRequest request) throws IOException {

        this.request = request;
        this.response = response;
        this.workContext = new WebWorkContext();
        this.session = request.getSslInfo();


        /**
         * 获取当前URL
         */
        this.workContext.setUrl(WebHelper.getUrl(request));


        /**
         * 获取IP
         */
        this.workContext.setIP(WebHelper.getIP(request));


        this.workContext.setUrlReferrer(WebHelper.getUrlReferrer(request));

    }


    @ModelAttribute
    public void inspectInitialize() {

        //System.out.println("inspectInitialize被执行了");
    }

    public BaseWebController() {
        super();
    }

}
