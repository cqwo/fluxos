/*
 *
 *  * Copyright (C) 2017.
 *  * 用于JAVA项目开发
 *  * 重庆英卡电子有限公司 版权所有
 *  * Copyright (C)  2017.  CqingWo Systems Incorporated. All rights reserved.
 *
 */

package com.cqwo.fluxos.web.framework.workcontext;

import com.cqwo.fluxos.core.CWMVersion;

public class BaseWorkContext {



    /**
     * 用户ip
     */
    private String IP = "127.0.0.1";//用户ip



    /**
     * 区域id
     */
    private int regionId = 0;//区域id

    /**
     * 当前url
     */
    private String url = "";//当前url

    /**
     * 上一次访问的url
     */
    private String urlReferrer = "";//上一次访问的url

    /**
     * 加密密码
     */
    private String encryptPwd = "";//加密密码



    /**
     * 页面标示符
     */
    private String pageKey = "homeaction";//页面标示符

    /**
     * 图片cdn
     */
    private String imageCDN = "/static/admin/images";//图片cdn

    /**
     * csscdn
     */
    private String cssCDN = "/static/admin/css";//csscdn

    /**
     * 脚本cdn
     */
    private String scriptCDN = "/static/admin/scripts";//脚本cdn

    /**
     * 字体cdn
     */
    private String fontCDN = "/static/admin/fonts";//字体cdn

    /**
     * 插件路径
     */
    private String pluginCDN = "/components";//插件路径

    /**
     * 在线总人数
     */
    private int onlineUserCount = 0;//在线总人数

    /**
     * 在线会员数
     */
    private int onlineMemberCount = 0;//在线会员数

    /**
     * 在线游客数
     */
    private int onlineGuestCount = 0;//在线游客数


    /**
     * 页面执行时间
     */
    private double executeTime = 0;//页面执行时间

    /**
     * 执行的sql语句数目
     */
    private int executeCount = 0;//执行的sql语句数目

    /**
     * 执行的sql语句细节
     */
    private String executeDetail = "";//执行的sql语句细节

    /**
     * 软件版本
     */
    private String version = CWMVersion.VERSION;//软件版本

    /**
     * 软件版权
     */
    private String copyright = CWMVersion.COPYRIGHT;//软件版权

    public BaseWorkContext() {
    }


    public BaseWorkContext(String IP, int regionId, String url, String urlReferrer, String encryptPwd, String pageKey, String imageCDN, String cssCDN, String scriptCDN, String fontCDN, String pluginCDN, int onlineUserCount, int onlineMemberCount, int onlineGuestCount, double executeTime, int executeCount, String executeDetail, String version, String copyright) {
        this.IP = IP;
        this.regionId = regionId;
        this.url = url;
        this.urlReferrer = urlReferrer;
        this.encryptPwd = encryptPwd;
        this.pageKey = pageKey;
        this.imageCDN = imageCDN;
        this.cssCDN = cssCDN;
        this.scriptCDN = scriptCDN;
        this.fontCDN = fontCDN;
        this.pluginCDN = pluginCDN;
        this.onlineUserCount = onlineUserCount;
        this.onlineMemberCount = onlineMemberCount;
        this.onlineGuestCount = onlineGuestCount;
        this.executeTime = executeTime;
        this.executeCount = executeCount;
        this.executeDetail = executeDetail;
        this.version = version;
        this.copyright = copyright;
    }


    public String getIP() {
        return IP;
    }

    public void setIP(String IP) {
        this.IP = IP;
    }

    public int getRegionId() {
        return regionId;
    }

    public void setRegionId(int regionId) {
        this.regionId = regionId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "BaseWorkContext{" +
                "IP='" + IP + '\'' +
                ", regionId=" + regionId +
                ", url='" + url + '\'' +
                ", urlReferrer='" + urlReferrer + '\'' +
                ", encryptPwd='" + encryptPwd + '\'' +
                ", pageKey='" + pageKey + '\'' +
                ", imageCDN='" + imageCDN + '\'' +
                ", cssCDN='" + cssCDN + '\'' +
                ", scriptCDN='" + scriptCDN + '\'' +
                ", fontCDN='" + fontCDN + '\'' +
                ", pluginCDN='" + pluginCDN + '\'' +
                ", onlineUserCount=" + onlineUserCount +
                ", onlineMemberCount=" + onlineMemberCount +
                ", onlineGuestCount=" + onlineGuestCount +
                ", executeTime=" + executeTime +
                ", executeCount=" + executeCount +
                ", executeDetail='" + executeDetail + '\'' +
                ", version='" + version + '\'' +
                ", copyright='" + copyright + '\'' +
                '}';
    }

    public String getUrlReferrer() {
        return urlReferrer;
    }

    public void setUrlReferrer(String urlReferrer) {
        this.urlReferrer = urlReferrer;
    }

    public String getEncryptPwd() {
        return encryptPwd;
    }

    public void setEncryptPwd(String encryptPwd) {
        this.encryptPwd = encryptPwd;
    }

    public String getPageKey() {
        return pageKey;
    }

    public void setPageKey(String pageKey) {
        this.pageKey = pageKey;
    }

    public String getImageCDN() {
        return imageCDN;
    }

    public void setImageCDN(String imageCDN) {
        this.imageCDN = imageCDN;
    }

    public String getCssCDN() {
        return cssCDN;
    }

    public void setCssCDN(String cssCDN) {
        this.cssCDN = cssCDN;
    }

    public String getScriptCDN() {
        return scriptCDN;
    }

    public void setScriptCDN(String scriptCDN) {
        this.scriptCDN = scriptCDN;
    }

    public String getFontCDN() {
        return fontCDN;
    }

    public void setFontCDN(String fontCDN) {
        this.fontCDN = fontCDN;
    }

    public String getPluginCDN() {
        return pluginCDN;
    }

    public void setPluginCDN(String pluginCDN) {
        this.pluginCDN = pluginCDN;
    }

    public int getOnlineUserCount() {
        return onlineUserCount;
    }

    public void setOnlineUserCount(int onlineUserCount) {
        this.onlineUserCount = onlineUserCount;
    }

    public int getOnlineMemberCount() {
        return onlineMemberCount;
    }

    public void setOnlineMemberCount(int onlineMemberCount) {
        this.onlineMemberCount = onlineMemberCount;
    }

    public int getOnlineGuestCount() {
        return onlineGuestCount;
    }

    public void setOnlineGuestCount(int onlineGuestCount) {
        this.onlineGuestCount = onlineGuestCount;
    }

    public double getExecuteTime() {
        return executeTime;
    }

    public void setExecuteTime(double executeTime) {
        this.executeTime = executeTime;
    }

    public int getExecuteCount() {
        return executeCount;
    }

    public void setExecuteCount(int executeCount) {
        this.executeCount = executeCount;
    }

    public String getExecuteDetail() {
        return executeDetail;
    }

    public void setExecuteDetail(String executeDetail) {
        this.executeDetail = executeDetail;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getCopyright() {
        return copyright;
    }

    public void setCopyright(String copyright) {
        this.copyright = copyright;
    }

}
