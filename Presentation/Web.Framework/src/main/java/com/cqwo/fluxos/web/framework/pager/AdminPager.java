/*
 *
 *  * Copyright (C) 2017.
 *  * 用于JAVA项目开发
 *  * 重庆英卡电子有限公司 版权所有
 *  * Copyright (C)  2017.  CqingWo Systems Incorporated. All rights reserved.
 *
 */

package com.cqwo.fluxos.web.framework.pager;

import com.cqwo.fluxos.web.framework.model.PageModel;

/**
 * Created by cqnews on 2017/12/14.
 */
public class AdminPager {

    private PageModel pageModel;

    public AdminPager(PageModel pageModel) {
        this.pageModel = pageModel;
    }

    @Override
    public String toString() {
        return "AdminPager{" +
                "pageModel=" + pageModel +
                '}';
    }
}
