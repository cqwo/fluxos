/*
 *
 *  *
 *  *  * Copyright (C) 2018.
 *  *  * 用于JAVA项目开发
 *  *  * 重庆英卡电子有限公司 版权所有
 *  *  * Copyright (C)  2018.  CqingWo Systems Incorporated. All rights reserved.
 *  *
 *
 */

package com.cqwo.fluxos.web.framework.view;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.cqwo.fluxos.core.message.MessageInfo;

/**
 * JSONView封装
 */
public class JsonView {


    //region JSONView封装

    /**
     * 返回JSON View
     *
     * @return
     */
    public static String view() {
        return view("英卡电子欢迎您");
    }

    /**
     * 返回JSON View
     * ServerHttpRequest
     *
     * @param message 消息
     * @return
     */
    public static String view(String message) {
        return view(-1, message);
    }

    /**
     * 返回JSON View
     *
     * @param state   状态
     * @param message 消息
     * @return
     */
    public static String view(Integer state, String message) {
        return view(state, null, message);
    }


    /**
     * 返回JSON View
     *
     * @param state   状态
     * @param content 内容
     * @param message 消息
     * @return
     */
    public static String view(Integer state, Object content, String message) {

//        Map<String, Object> map=new HashMap<String, Object>();

//        response.setCharacterEncoding("UTF-8");
//        response.setContentType("application/json; charset=utf-8");
//        response.setHeader("Access-Control-Allow-Origin", "*");
//        response.setHeader("Access-Control-Allow-Credentials", "true");
//        response.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Connection, User-Agent, Cookie");
//        response.setStatus(200);


        MessageInfo messageInfo = new MessageInfo();
        messageInfo.setState(state);
        messageInfo.setContent(content);
        messageInfo.setMessage(message);


        return JSON.toJSONString(messageInfo, SerializerFeature.DisableCircularReferenceDetect);

    }
    //endregion
}
