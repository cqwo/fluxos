/*
 *
 *  * Copyright (C) 2017.
 *  * 用于JAVA项目开发
 *  * 重庆英卡电子有限公司 版权所有
 *  * Copyright (C)  2017.  CqingWo Systems Incorporated. All rights reserved.
 *
 */

package com.cqwo.fluxos.web.framework.controller;

import com.cqwo.fluxos.core.helper.StringHelper;
import com.cqwo.fluxos.core.helper.TypeHelper;
import com.cqwo.fluxos.web.framework.view.JsonView;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.http.server.reactive.SslInfo;



public abstract class BaseController {



    protected ServerHttpRequest request;

    protected ServerHttpResponse response;

    protected SslInfo session;


    protected String urlReferer = "/";


    protected BaseController() {
    }

    abstract Object getWorkContext();


    /**
     * 返回JSON View
     *
     * @return String
     */
    public String jsonView() {
        return jsonView("英卡电子欢迎您");
    }

    /**
     * 返回JSON View
     *
     * @param message 消息
     * @return String
     */
    public String jsonView(String message) {
        return jsonView(-1, message);
    }

    /**
     * 返回JSON View
     *
     * @param state   状态
     * @param message 消息
     * @return String
     */
    public String jsonView(Integer state, String message) {
        return jsonView(state, null, message);
    }


    /**
     * 返回JSON View
     *
     * @param state   状态
     * @param content 内容
     * @param message 消息
     * @return String
     */
    public String jsonView(Integer state, Object content, String message) {

        return JsonView.view(state, content, message);

    }


    /**
     * 获得路由中的值
     *
     * @param key 关键词
     * @return int
     */
    protected Integer getParameterInt(String key) {
        return getParameterInt(key, 0);
    }


    /**
     * 获得路由中的值
     *
     * @param key          关键词
     * @param defaultValue 默认值
     * @return int
     */
    protected Integer getParameterInt(String key, Integer defaultValue) {
        return TypeHelper.stringToInt(request.getQueryParams().getFirst(key), defaultValue);
    }


    /**
     * 获得路由中的值
     *
     * @param key 关键词
     * @return int
     */
    protected String getParameterString(String key) {
        return getParameterString(key, "");
    }


    /**
     * 获得路由中的值
     *
     * @param key          关键词
     * @param defaultValue 默认值
     * @return int
     */
    protected String getParameterString(String key, String defaultValue) {


        String str = String.valueOf(request.getQueryParams().getFirst(key));

        if (StringHelper.isBlank(str)) {
            return defaultValue;
        }
        return str.toString();
    }

    /**
     * 从header中获取值
     *
     * @param key
     * @return
     */
    protected Integer getHeaderInt(String key) {
        return getHeaderInt(key, 0);
    }

    /**
     * 从header中获取值
     *
     * @param key key
     * @param defaultValue defaultValue
     * @return
     */
    protected Integer getHeaderInt(String key, Integer defaultValue) {

        return TypeHelper.stringToInt(request.getHeaders().get(key), defaultValue);
    }

    /**
     * 获得路由中的值
     *
     * @param key 关键词
     * @return int
     */
    protected String getHeaderString(String key) {
        return getHeaderString(key, "");
    }


    /**
     * 获得路由中的值
     *
     * @param key          关键词
     * @param defaultValue 默认值
     * @return int
     */
    protected String getHeaderString(String key, String defaultValue) {
        String str = String.valueOf(request.getHeaders().get(key));

        if (StringHelper.isBlank(str)) {
            return defaultValue;
        }
        return str.toString();
    }

    /**
     * 跳转页面
     *
     * @param url 跳转地址
     */
    protected void sendRedirect(String url) {
        sendRedirect(url, "/");
    }


    /**
     * 跳转页面 不实现了
     *
     * @param url      跳转地址
     * @param errorUrl 错误地址
     */
    @Deprecated
    protected void sendRedirect(String url, String errorUrl) {

    }


}
