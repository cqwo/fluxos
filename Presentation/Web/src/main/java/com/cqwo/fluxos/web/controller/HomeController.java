package com.cqwo.fluxos.web.controller;


import com.cqwo.fluxos.web.framework.controller.BaseWebController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController(value = "HomeController")
public class HomeController extends BaseWebController {



    @RequestMapping(value = "/")
    public String index() {
        return "welcome to 510link.com";
    }




    /**
     * hello
     *
     * @return
     */
    @RequestMapping(value = "/hello")
    public String hello() {


        return "我喜欢您";
    }
}
