package com.cqwo.fluxos.web.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author cqnews
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReceiveModel implements Serializable {

    private static final long serialVersionUID = 7657867788709868628L;
    /**
     * 租户ID
     */
    private String tenantId = "";

    /**
     * 产品ID
     */
    private String productId = "";

    /**
     * 设备ID
     */
    private String deviceId = "";

    /**
     * 消息类型=dataReport
     */
    private String messageType = "";

    /**
     * NB终端设备识别号
     */
    private String IMEI = "";

    /**
     * NB终端sim卡标识
     */
    private String IMSI = "";

    /**
     * 设备标识
     */
    private String deviceType = "";

    /**
     * 数据上报主题
     */
    private String topic = "";

    /**
     * 合作伙伴ID
     */
    private String assocAssetId = "";

    /**
     * 时间戳
     */
    private long timestamp = 0;

    /**
     * 上行报文序号
     */
    private String upPacketSN = "";

    /**
     * 数据上报报文序号
     */
    private String upDataSN = "";

    /**
     * 服务ID
     */
    private String datasetId = "";

    /**
     * 协议类型
     */
    private String protocol = "";

    /**
     * 消息负载
     */
    private String payload = "";
}
