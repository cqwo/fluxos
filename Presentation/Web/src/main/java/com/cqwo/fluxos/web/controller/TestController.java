package com.cqwo.fluxos.web.controller;

import com.cqwo.fluxos.web.model.ReceiveModel;
import org.junit.Test;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.concurrent.TimeUnit;

@RestController
public class TestController {

    @Test
    public void webClientTest1() throws InterruptedException {
        WebClient webClient = WebClient.create("http://www.baidu.com/");
        Mono<String> resp = webClient
                .get()
                .retrieve()
                .bodyToMono(String.class);
        resp.subscribe(System.out::println);

        TimeUnit.SECONDS.sleep(1);
    }


}
