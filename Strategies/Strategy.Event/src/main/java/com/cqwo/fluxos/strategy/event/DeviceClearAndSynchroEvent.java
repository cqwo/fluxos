//package com.link510.fluxos.strategy.event;
//
//
//import IEvent;
//import com.link510.fluxos.services.GateWays;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.scheduling.annotation.Scheduled;
//import org.springframework.stereotype.Component;
//
//@Component(value = "DeviceClearAndSynchroEvent")
//public class DeviceClearAndSynchroEvent implements IEvent {
//
//    @Autowired
//    GateWays devices;
//
//    @Override
//    @Scheduled(cron = "0 30 00 * * ?")
//    public void execute() {
//        //devices.clearAndSynchroGateWayList();
//    }
//
//
//}
