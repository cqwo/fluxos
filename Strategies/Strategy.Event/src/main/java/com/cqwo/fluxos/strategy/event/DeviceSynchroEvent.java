//package com.link510.fluxos.strategy.event;
//
//
//import IEvent;
//import com.link510.fluxos.services.GateWays;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.scheduling.annotation.Scheduled;
//import org.springframework.stereotype.Component;
//
//@Component(value = "DeviceSynchroEvent")
//public class DeviceSynchroEvent implements IEvent {
//
//    @Autowired
//    GateWays devices;
//
//    @Override
//    @Scheduled(cron = "0 0/10 * * * ?")
//    public void execute() {
//        devices.synchroGateWayList();
//    }
//
//
//}
