package com.cqwo.fluxos.strategy.kafka;


import com.cqwo.fluxos.core.queue.IQueueStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.ListenableFuture;

import java.util.UUID;

@Component(value = "KafkaStrategy")
@EnableScheduling
public class QueueKafkaStrategy implements IQueueStrategy {


    @Autowired
    private KafkaTemplate template;


    /**
     * 发送消息
     * @param topic
     * @param keys
     * @param msg
     */
    @Override
    public void send(String topic, String keys, String msg) {

        keys += UUID.randomUUID();

        ListenableFuture future = this.template.send(topic, keys, msg);
        future.addCallback(o -> System.out.println("send-消息发送成功：" + msg), throwable -> System.out.println("消息发送失败：" + msg));

    }


//    /**
//     * //     * 定时任务
//     * //
//     */
//    @Scheduled(cron = "0/1 * * * * ?")
//    public void send() {
//
//        String keys = UUID.randomUUID().toString();
//        String sendMessage = "I'm messageInfo;" + keys;
//        ListenableFuture future = this.template.send("myTopic", keys, sendMessage);
//        future.addCallback(o -> System.out.println("send-消息发送成功：" + sendMessage), throwable -> System.out.println("消息发送失败：" + sendMessage));
//
//    }
}
