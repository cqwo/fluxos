package com.cqwo.fluxos.startegy.rabbit.config;

import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitConfig {

    /**
     * 注入配置文件属性
     */
    @Value("${spring.rabbitmq.host}")
    String host;//MQ地址

    @Value("${spring.rabbitmq.username}")
    String username;//MQ登录名

    @Value("${spring.rabbitmq.password}")
    String password;//MQ登录密码


    //创建mq连接
    @Bean(name = "connectionFactory")
    public ConnectionFactory connectionFactory() {
        CachingConnectionFactory connectionFactory = new CachingConnectionFactory();

        connectionFactory.setUsername(username);
        connectionFactory.setPassword(password);
        connectionFactory.setPublisherConfirms(true);

        //该方法配置多个host，在当前连接host down掉的时候会自动去重连后面的host
        connectionFactory.setAddresses(host);

        return connectionFactory;
    }

}
