package com.cqwo.fluxos.startegy.rabbit;

import com.cqwo.fluxos.core.queue.IQueueStrategy;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;


/**
 * RabbitMQ消息队列
 */
@Component(value = "QueueRabbitStrategy")
@Primary
public class RabbitStrategy implements IQueueStrategy {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private ConnectionFactory connectionFactory;

    /**
     * 发送rabbiot 队列
     *
     * @param exchange 队列名
     * @param keys     关键词
     * @param msg      消息
     */
    @Override
    public void send(String exchange, String keys, String msg) {
        //System.out.println("Sender : " + msg);
        rabbitTemplate.convertAndSend(exchange, keys, msg);
    }

    /**
     * 创建消息队列
     *
     * @param queueName    队列名称
     * @param exchangeName 交换机名称
     */
    @Override
    public void createQueue(String queueName, String exchangeName) {
        createQueue(queueName, exchangeName, queueName);
    }


    /**
     * 创建消息队列
     *
     * @param queueName    队列名称
     * @param exchangeName 交换机名称
     * @param exchangeName 路由key
     */
    @Override
    public void createQueue(String queueName, String exchangeName, String routingKey) {

        try {
            connectionFactory.createConnection().createChannel(false).queueDeclare(queueName, true, false, false, null);
            connectionFactory.createConnection().createChannel(false).queueBind(queueName, exchangeName, queueName);

        } catch (Exception ex) {
            //e.printStackTrace();
        }

    }

    @Override
    public boolean existQueue(String queueName, String exchangeName) {

        try {
            connectionFactory.createConnection().createChannel(false).queueBind(queueName, exchangeName, queueName);
            return true;
        } catch (Exception ex) {

            //e.printStackTrace();
        }
        return false;
    }
}
