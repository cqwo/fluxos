/*
 * Copyright (c) 2018. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.cqwo.fluxos.core.enums;

import com.cqwo.fluxos.core.helper.TypeHelper;
import com.cqwo.fluxos.core.model.SelectListItem;

import java.util.ArrayList;
import java.util.List;

/**
 * 消息等级
 */
public enum MsgLevel {

    /**
     * 消息等级
     */
    Operate(-2, "操作", "purple"),
    Other(-1, "未分类", "gray"),
    Heart(0, "心跳", "blue"),
    Data(1, "数据", "green"),
    Warning(2, "警告", "red");

    private Integer level = 0;

    private String name = "";

    private String color = "";


    MsgLevel(Integer level, String name, String color) {
        this.level = level;
        this.name = name;
        this.color = color;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "MsgLevel{" +
                "level=" + level +
                ", name='" + name + '\'' +
                ", color='" + color + '\'' +
                '}';
    }

    public static String getName(Integer level) {

        for (MsgLevel level2 : values()) {

            if (level.equals(level2.getLevel())) {
                return level2.getName();
            }
        }

        return Other.getName();
    }


    public static MsgLevel getMessageLevel(Integer level) {

        for (MsgLevel level2 : values()) {

            if (level.equals(level2.getLevel())) {
                return level2;
            }
        }

        return Other;
    }


    /**
     * 获取枚举的列表
     *
     * @return
     */
    public static List<SelectListItem> getSelectListItem() {
        return getSelectListItem(Other.getLevel());
    }


    /**
     * 获取枚举的列表
     * index 默认选择项目
     *
     * @return
     */
    public static List<SelectListItem> getSelectListItem(Integer level) {

        List<SelectListItem> selectListItemList = new ArrayList<>();

        for (MsgLevel level2 : values()) {

            SelectListItem item = new SelectListItem();

            item.setText(level2.getName());
            item.setValue(TypeHelper.intToString(level2.getLevel()));
            item.setColor(level2.getColor());

            if (level2.getLevel().equals(level)) {
                item.setSelected(true);
            }

            selectListItemList.add(item);
        }

        return selectListItemList;
    }

}
