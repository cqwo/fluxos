package com.cqwo.fluxos.core.message;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 数据消息
 *
 * @author cqnews
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DataMessageInfo implements Serializable {

    private static final long serialVersionUID = -7542857831995161742L;

    /**
     * 头文件
     */
    private String head = "";

    /**
     * 数据部分
     */
    private String data = "";

    /**
     * 包尾
     */
    private String end = "";


}
