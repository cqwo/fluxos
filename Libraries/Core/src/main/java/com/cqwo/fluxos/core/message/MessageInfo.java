/*
 *
 *  *
 *  *  * Copyright (C) 2018.
 *  *  * 用于JAVA项目开发
 *  *  * 重庆青沃科技有限公司 版权所有
 *  *  * Copyright (C)  2018.  CqingWo Systems Incorporated. All rights reserved.
 *  *
 *
 */

package com.cqwo.fluxos.core.message;

import com.alibaba.fastjson.JSON;
import lombok.*;

import java.io.Serializable;


/**
 * @author cqnews
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MessageInfo implements Serializable {

    private static final long serialVersionUID = 6884040831450773569L;

    //private Logger logger = LoggerFactory.getLogger(MessageInfo.class);

    /**
     * 消息分类
     */
    private Integer type = 0;


    /**
     * 消息状态
     */
    private Integer state = 0;

    /**
     * 消息说明
     */
    private String message = "英卡电子欢迎您";


    /**
     * 消息正文
     */
    private Object content;


    public MessageInfo(String message) {
        this.message = message;
    }


    public MessageInfo(Integer type, Integer state, String message) {
        this.type = type;
        this.state = state;
        this.message = message;
    }


    public String toJson() {
        return JSON.toJSONString(this);
    }

    /**
     * 消息组装
     *
     * @param msg 消息文本
     * @return
     */
    public static MessageInfo of(String msg) throws Exception {

        try {

            return JSON.parseObject(msg, MessageInfo.class);

        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        }
    }
}
