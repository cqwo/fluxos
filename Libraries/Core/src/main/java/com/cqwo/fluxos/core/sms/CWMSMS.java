/*
 *
 *  * Copyright (C) 2017.
 *  * 用于JAVA项目开发
 *  * 重庆英卡电子有限公司 版权所有
 *  * Copyright (C)  2017.  CqingWo Systems Incorporated. All rights reserved.
 *
 */

package com.cqwo.fluxos.core.sms;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by cqnews on 2017/3/20.
 */
@Service(value = "CWMSMS")
@Setter
@Getter
public class CWMSMS {

    /**
     * 短信策略
     */
    @Autowired(required = false)
    private ISMSStrategy ismsStrategy;

}

