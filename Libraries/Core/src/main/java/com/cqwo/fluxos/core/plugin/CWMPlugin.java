/*
 *
 *  *
 *  *  * Copyright (C) 2018.
 *  *  * 用于JAVA项目开发
 *  *  * 重庆英卡电子有限公司 版权所有
 *  *  * Copyright (C)  2018.  CqingWo Systems Incorporated. All rights reserved.
 *  *
 *
 */

package com.cqwo.fluxos.core.plugin;

import com.cqwo.fluxos.core.context.SpringContext;
import com.cqwo.fluxos.core.plugin.interface2.IPlugin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Map;


@Component(value = "CWMPlugin")
public class CWMPlugin {

    @Autowired
    SpringContext springConext;


    /**
     * 定时器列表
     */
    private Map<String, IPlugin> pluginList;




    /**
     * 初始化插件
     */
    @PostConstruct
    public void init() {
        this.pluginList = SpringContext.getApplicationContext().getBeansOfType(IPlugin.class);
    }


    /**
     * 所有插件列表
     *
     * @return
     */
    public Map<String, IPlugin> pluginList() {
        return pluginList;
    }

}
