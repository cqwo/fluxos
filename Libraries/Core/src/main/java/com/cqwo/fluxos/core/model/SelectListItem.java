package com.cqwo.fluxos.core.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/*
 *
 *  * Copyright (C) 2017.
 *  * 用于JAVA项目开发
 *  * 重庆英卡电子有限公司 版权所有
 *  * Copyright (C)  2017.  CqingWo Systems Incorporated. All rights reserved.
 *
 */

/**
 * @author cqnews
 * @date 2017/12/20
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SelectListItem implements Serializable {

    private static final long serialVersionUID = 1191419740692652428L;
    /**
     * 如果选定此项，则为 true；否则为 false。
     */
    public boolean selected = false;

    /**
     * 文本
     */
    public String text = "";

    /**
     * 值
     */
    public String value = "";

    /**
     * 颜色
     */
    public String color = "";


    public SelectListItem(boolean selected, String text, String value) {
        this.selected = selected;
        this.text = text;
        this.value = value;
    }

}


