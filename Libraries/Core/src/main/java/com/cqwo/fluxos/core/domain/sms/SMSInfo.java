/*
 *
 *  * Copyright (C) 2017.
 *  * 用于JAVA项目开发
 *  * 重庆英卡电子有限公司 版权所有
 *  * Copyright (C)  2017.  CqingWo Systems Incorporated. All rights reserved.
 *
 */

package com.cqwo.fluxos.core.domain.sms;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

/**
 * Created by cqnews on 2017/3/23.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SMSInfo implements Serializable {

    private static final long serialVersionUID = -2181654979352120507L;
    /**
     * 短信ID
     */
    private Integer id = 0; // 短信ID
    /**
     * 用户ID
     */
    private Integer uid = 1; // 用户ID
    /**
     * 短信类型
     */
    private Integer type = 0; // 短信类型
    /**
     * Code
     */
    private String code = ""; // Code
    /**
     * 发送的地址
     */
    private String mobile = ""; // 发送的地址
    /**
     * 发送内容
     */
    private String body = ""; // 发送内容
    /**
     * 手机发送短信时间
     */
    private Integer sendTime = 0; // 手机发送短信时间


    @Override
    public String toString() {
        return "SMSInfo{" +
                "id=" + id +
                ", uid=" + uid +
                ", type=" + type +
                ", code='" + code + '\'' +
                ", mobile='" + mobile + '\'' +
                ", body='" + body + '\'' +
                ", sendTime=" + sendTime +
                '}';
    }
}
