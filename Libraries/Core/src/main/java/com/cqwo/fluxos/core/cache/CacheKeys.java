/*
 *
 *  * Copyright (C) 2017.
 *  * 用于JAVA项目开发
 *  * 重庆英卡电子有限公司 版权所有
 *  * Copyright (C)  2017.  CqingWo Systems Incorporated. All rights reserved.
 *
 */

package com.cqwo.fluxos.core.cache;

/**
 * Created by cqnews on 2017/12/14.
 * 缓存键
 */
public class CacheKeys {


    /**
     * 设备列表
     */
    public final static String SYNCHRO_GATEWAY_LIST = "synchrogatewaylist";


    public final static String GET_GATE_WAY_LIST = "getgatewaylist";

    /**
     * 通过tokenhuoqu
     */
    public final static String GET_GATE_WAY_BY_TOKEN = "getgatewaybytoken";

    /**
     * 在线设备
     */
    public final static String ONLINE_GATE_WAY = "onlinegateway";

    /**
     * 在线设备,通过网关
     */
    public final static String ONLINE_GATE_WAY_TOKEN = "onlinegatewaytoken";

    public static final String ONLINE_GATE_WAY_CHANNELID = "onlinegatewaychannelid";

    /**
     * 判断缓存是否存在
     */
    public static final String IS_MESSAGE_CACHE_EXIST = "ismessagecacheexist";

    public static final String SPLIT = "_";
}
