/*
 *
 *  *
 *  *  * Copyright (C) 2018.
 *  *  * 用于JAVA项目开发
 *  *  * 重庆英卡电子有限公司 版权所有
 *  *  * Copyright (C)  2018.  CqingWo Systems Incorporated. All rights reserved.
 *  *
 *
 */

package com.cqwo.fluxos.core.message;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

/**
 * @author cqnews
 */
@ToString
@AllArgsConstructor
public enum MessageType {

    /**
     * 消息类型
     */

    ERROR("错误消息", -1),
    MESSAGE("消息", 0),
    HEART("心跳", 1),
    REGISTER("注册", 2),
    DEVICE("设备", 3),
    Command("命令", 4);

    // 成员变量
    @Getter
    private String name;

    @Getter
    private Integer index;




    // 普通方法
    public static String getName(Integer index) {
        for (MessageType m : MessageType.values()) {
            if (m.getIndex().equals(index)) {
                return m.name;
            }
        }
        return MESSAGE.name;
    }


    public static MessageType getMessageType(Integer index) {

        for (MessageType m : MessageType.values()) {
            if (m.getIndex().equals(index)) {
                return m;
            }
        }
        return ERROR;
    }
}
