/*
 *
 *  *
 *  *  * Copyright (C) 2018.
 *  *  * 用于JAVA项目开发
 *  *  * 重庆英卡电子有限公司 版权所有
 *  *  * Copyright (C)  2018.  CqingWo Systems Incorporated. All rights reserved.
 *  *
 *
 */

package com.cqwo.fluxos.core.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author cqnews
 */
@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MessageException extends Exception {


    private static final long serialVersionUID = 786640981165579536L;

    /**
     * 消息状态
     */
    private Integer state = 0;

    /**
     * 消息说明
     */
    private String message = "英卡电子欢迎您";


    /**
     * 消息正文
     */
    private Object content;

    public MessageException(String message) {
        this.message = message;
    }


    public MessageException(Integer state, String message) {
        this.state = state;
        this.message = message;
    }


}
