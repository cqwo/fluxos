package com.cqwo.fluxos.core.config;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.io.Serializable;

/**
 * @author cqnews
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ConfigurationProperties(prefix = "spring.application")
@Component(value = "AppConfig")
public class AppConfig implements Serializable {

    private static final long serialVersionUID = 5056333878183367752L;

    /**
     * 项目名称
     */
    private String name = "";

    /**
     * 项目域名地址
     */
    private String domain = "";


    /**
     * 项目ip地址
     */
    private String ip = "";


    /**
     * 管理员列表
     */
    private String[] admin = new String[]{"cqnews"};

}
