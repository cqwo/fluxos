package com.cqwo.fluxos.core.helper;

import static com.cqwo.fluxos.core.helper.ByteHelper.data32todata8;

public class CrcHelper {

    private final static String keyStr = "00000001000000020000000300000004";

    /**
     * key+time的crc计算
     */
    public static String keyTimeCrc(String key, String timestamp) {
        int len = timestamp.length() + 16;
        int dwPolynomial = 0x04c11db7;
        short bits;
        long xbit;
        int data;
        int CRC1 = 0xFFFFFFFF;
        int n = 0;
        int[] ptr = new int[len];
        char[] time = timestamp.toCharArray();

//    	long[] a = TeaSupport.data8todata32(key.toCharArray());
        int index = 0;
        for (int i = 0; i < 4; i++) {
            for (int j = 4; j > 0; j--) {
                ptr[index] = Integer.parseInt((key.substring(i * 8, i * 8 + 8)).substring(j * 2 - 2, j * 2), 16);
                index++;
            }
        }

        for (int i = 0; i < timestamp.length(); i++) {
            ptr[index] = time[index - 16];
            index++;
        }

        while (len-- != 0) {
            xbit = 2147483648L;
            data = ptr[n];
            for (bits = 0; bits < 32; bits++) {
                if ((CRC1 & 0x80000000) != 0) {
                    CRC1 <<= 1;
                    CRC1 ^= dwPolynomial;
                } else {
                    CRC1 <<= 1;
                }
                if ((data & xbit) != 0) {
                    CRC1 ^= dwPolynomial;
                }
                xbit >>= 1;
            }
            n++;
        }
        String result = Integer.toHexString(CRC1);
        while (result.length() < 8) {
            result = "0" + result;
        }
        return result;
    }

    /**
     * mark+time的crc计算
     */
    public static String markTimeCrc(String mark, String timestamp) {
        String str = mark + timestamp;
        int len = str.length();
        int dwPolynomial = 0x04c11db7;
        short bits;
        long xbit;
        int data;
        int CRC1 = 0xFFFFFFFF;
        int n = 0;
        char[] ptr = str.toCharArray();

        while (len-- != 0) {
            xbit = 2147483648L;
            data = ptr[n];
            for (bits = 0; bits < 32; bits++) {
                if ((CRC1 & 0x80000000) != 0) {
                    CRC1 <<= 1;
                    CRC1 ^= dwPolynomial;
                } else {
                    CRC1 <<= 1;
                }
                if ((data & xbit) != 0) {
                    CRC1 ^= dwPolynomial;
                }
                xbit >>= 1;
            }
            n++;
        }
        String result = Integer.toHexString(CRC1);
        while (result.length() < 8) {
            result = "0" + result;
        }
        return result;
    }

    /**
     * bin文件的crc计算
     */
    public static String markTimeCrc(byte[] binfile) {
        int len = binfile.length;
        int dwPolynomial = 0x04c11db7;
        short bits;
        long xbit;
        int data;
        int CRC1 = 0xFFFFFFFF;
        int n = 0;

        while (len-- != 0) {
            xbit = 2147483648L;
            if (binfile[n] < 0) {
                data = binfile[n] + 256;
            } else {
                data = binfile[n];
            }
            for (bits = 0; bits < 32; bits++) {
                if ((CRC1 & 0x80000000) != 0) {
                    CRC1 <<= 1;
                    CRC1 ^= dwPolynomial;
                } else {
                    CRC1 <<= 1;
                }
                if ((data & xbit) != 0) {
                    CRC1 ^= dwPolynomial;
                }
                xbit >>= 1;
            }
            n++;
        }
        String result = Integer.toHexString(CRC1);
        while (result.length() < 8) {
            result = "0" + result;
        }
        return result;
    }

    /**
     * 创建20位mark随机数
     */
    public static String buildMark() {
        String str = "";
        for (int i = 0; i < 16; i++) {
            str += String.valueOf((int) (Math.random() * 10));
        }
        return str;
    }

    public static long[] separateKey(String key) {
        long[] lon = new long[4];
        for (int i = 0; i < 4; i++) {
            lon[i] = Long.parseLong(key.substring(i * 8, i * 8 + 8), 16);
        }
        char[] b = data32todata8(lon);


        return lon;

    }


    /**
     * 页面进制数转字符
     */
    public static String changeKey(String key) {
        String changekey = "";
        long[] lon = new long[4];
        for (int i = 0; i < 4; i++) {
            lon[i] = Long.parseLong(key.substring(i * 8, i * 8 + 8), 16);
        }
        char[] b = data32todata8(lon);
        for (int i = 0; i < b.length; i++) {
            changekey += b[i];
        }
        return changekey;
    }

    /**
     * byte转二进制补0
     */
    public static String addZero(byte byt) {
        String str = Integer.toBinaryString(0xFF & byt);
        int length = str.length();
        if (length < 8) {
            for (int i = 0; i < 8 - length; i++) {
                str = "0" + str;
            }
        }
        return str;
    }

    /**
     * byte转16进制补0
     */
    public static String addHexZero(byte byt) {
        String str = Integer.toHexString(0xFF & byt);
        int length = str.length();
        if (length == 1) {
            str = "0" + str;
        }
        return str;
    }

    /**
     * 16转byte
     */
    public static byte hexToBy(char ch1, char ch2) {
        return (byte) (((byte) ch1) << 4 | ((byte) ch2));
    }

    /**
     * char转byte
     */
    public static byte charToby(String str) {
        return hexToBy(str.charAt(0), str.charAt(1));
    }

    /**
     * plength补0
     */
    public static String plengthAddZero(long x) {
        String str = Long.toHexString(x);
        int length = str.length();
        if (length < 8) {
            for (int i = 0; i < 8 - length; i++) {
                str = "0" + str;
            }
        }
        return str;
    }


    public static void main(String[] args) {

        //System.out.println(keyTimeCrc(keyStr, "00000020180706150956"));


        //System.out.println(markTimeCrc("8964909585076858", "00000020180719165039"));
    }


}
