package com.cqwo.fluxos.core.envent;


import com.cqwo.fluxos.core.context.SpringContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Map;


@Service(value = "CWMEvent")
public class CWMEvent {


    @Autowired
    private SpringContext springConext;

    /**
     * 定时器列表
     */
    private Map<String, IEvent> eventLit;

    /**
     * 注入所以实现了Strategy接口的Bean
     *
     * @param
     */
    public CWMEvent() {
    }

    @PostConstruct
    public void init() {
        this.eventLit = SpringContext.getApplicationContext().getBeansOfType(IEvent.class);
    }


    /**
     * 插件列表
     *
     * @return
     */
    public Map<String, IEvent> eventList() {
        return this.eventLit;
    }

}


