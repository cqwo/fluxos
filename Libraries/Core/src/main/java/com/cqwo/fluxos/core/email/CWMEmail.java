/*
 *
 *  * Copyright (C) 2017.
 *  * 用于JAVA项目开发
 *  * 重庆英卡电子有限公司 版权所有
 *  * Copyright (C)  2017.  CqingWo Systems Incorporated. All rights reserved.
 *
 */

package com.cqwo.fluxos.core.email;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by cqnews on 2017/4/16.
 */

@Service(value = "CWMEmail")
@Setter
@Getter
public class CWMEmail {

    /**
     * 邮件策略
     */
    @Autowired(required = false)
    private IEmailStrategy iEmailStrategy;


}
