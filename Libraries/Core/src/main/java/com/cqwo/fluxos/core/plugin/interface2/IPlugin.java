/*
 *
 *  *
 *  *  * Copyright (C) 2018.
 *  *  * 用于JAVA项目开发
 *  *  * 重庆英卡电子有限公司 版权所有
 *  *  * Copyright (C)  2018.  CqingWo Systems Incorporated. All rights reserved.
 *  *
 *
 */

package com.cqwo.fluxos.core.plugin.interface2;

import javax.annotation.PostConstruct;

/**
 * 插件接口
 */
public interface IPlugin {

    /**
     * 类名
     *
     * @return
     */
    String name();

    /**
     * 说明
     *
     * @return
     */
    String desc();


    /**
     * 插件版本
     *
     * @return
     */
    String version();


    /**
     * 初始化插件
     */
    @PostConstruct
    void initPlugin() throws Exception;
}
