/*
 *
 *  * Copyright (C) 2017.
 *  * 用于JAVA项目开发
 *  * 重庆英卡电子有限公司 版权所有
 *  * Copyright (C)  2017.  CqingWo Systems Incorporated. All rights reserved.
 *
 */

package com.cqwo.fluxos.core.log;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.MessageFormat;

/**
 * Created by cqnews on 2017/4/15.
 */
@Service(value = "CWMLogs")
@Getter
public class CWMLogs {

    @Autowired
    private ILogStrategy iLogStrategy;

    /**
     * 写入日志
     *
     * @param message 消息
     */
    public void write(String message) {
        try {
            iLogStrategy.write(message);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * 写入日志
     *
     * @param ex 异常对象
     */
    public void write(Exception ex) {

        try {
            iLogStrategy.write(MessageFormat.format("方法:{0},异常信息:{1}", ex.getStackTrace(), ex.getMessage()));
        } catch (Exception e) {
            ex.printStackTrace();
        }
    }

    /**
     * 写入日志
     *
     * @param ex      异常
     * @param message 消息
     */
    public void write(Exception ex, String message) {

        try {
            iLogStrategy.write(MessageFormat.format("异常描述:{0},方法:{1},异常信息:{2}", message, ex.getStackTrace(), ex.getMessage()));
        } catch (Exception e) {
            ex.printStackTrace();
        }

    }


    /**
     * debug输出
     *
     * @param meassage 消息
     */
    public void debug(String meassage) {
        try {
            iLogStrategy.debug(meassage);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * info输出
     *
     * @param meassage 消息
     */
    public void info(String meassage) {
        try {
            iLogStrategy.info(meassage);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * warn输出
     *
     * @param meassage 消息
     */
    public void warn(String meassage) {
        try {
            iLogStrategy.warn(meassage);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * error输出
     *
     * @param meassage 消息
     */
    public void error(String meassage) {
        try {
            iLogStrategy.error(meassage);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * fatal输出
     *
     * @param meassage 消息
     */
    public void fatal(String meassage) {
        try {
            iLogStrategy.fatal(meassage);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
