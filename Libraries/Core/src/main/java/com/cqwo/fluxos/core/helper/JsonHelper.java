/*
 *
 *  * Copyright (C) 2017.
 *  * 用于JAVA项目开发
 *  * 重庆英卡电子有限公司 版权所有
 *  * Copyright (C)  2017.  CqingWo Systems Incorporated. All rights reserved.
 *
 */

package com.cqwo.fluxos.core.helper;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by cqnews on 2017/3/20.
 */
public class JsonHelper {


    static Logger logger = LoggerFactory.getLogger(JsonHelper.class);

    /**
     * 将数组转换成JSON
     *
     * @param object
     * @return
     */
    public static JSON Object2Json(Object object) {
        return (JSON) JSON.toJSON(object);
    }

    /**
     * 将Json串转成JSONObject
     *
     * @param json json串
     * @return
     */
    public static JSONObject Json2Object(String json) {

        JSONObject jsonObject = JSON.parseObject(json);

        return jsonObject;

    }

    /**
     * 将json转成map
     * @param json
     * @return
     */
    public static Map getMap4Json(String json){
        JSONObject jsonObject = JSONObject.parseObject(json);
        Set<String> keys = jsonObject.keySet();
        Object value;
        Map valueMap = new HashMap();
        for (String key:keys) {
            value = jsonObject.get(key);
            valueMap.put(key, value);
        }

        return valueMap;
    }


    /**
     * 在ObjectJson中获取值
     *
     * @param dataJson     json
     * @param key          key
     * @param defaultValue 默认值
     * @return
     */
    public static String getString(JSONObject dataJson, String key, String defaultValue) {


        String s = null;

        try {

            s = dataJson.getString(key);

        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }

        if (s == null) {
            s = defaultValue;
        }

        return s;
    }

    public static String getString(JSONObject dataJson, String from) {
        return getString(dataJson, from, "");
    }
}
