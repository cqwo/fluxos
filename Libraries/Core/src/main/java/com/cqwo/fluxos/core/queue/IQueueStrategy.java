package com.cqwo.fluxos.core.queue;


/**
 * kafkat生产者策略
 */
public interface IQueueStrategy {

    /**
     * 发送消息
     *
     * @param topic
     * @param keys
     * @param msg
     */
    void send(String topic, String keys, String msg);

    /**
     * 创建消息队列
     *
     * @param queueName    队列名称
     * @param exchangeName 交换机名称
     */
    default void createQueue(String queueName, String exchangeName) {

    }

    /**
     * 创建队列
     *
     * @param queueName    队列名
     * @param exchangeName 交换机的名字
     * @param exchangeName 路由key
     */
    default void createQueue(String queueName, String exchangeName, String routingKey) {

    }

    /**
     * 判断队列是否存在
     *
     * @param queueName 队列名称
     * @return 队列是否存在
     */
    default boolean existQueue(String queueName, String exchangeName) {
        return false;
    }
}
