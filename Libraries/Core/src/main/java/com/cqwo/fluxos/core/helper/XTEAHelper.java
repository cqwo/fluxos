package com.cqwo.fluxos.core.helper;


import static com.cqwo.fluxos.core.helper.ByteHelper.*;

public class XTEAHelper {


    private static final long DELTA = getUint32(0x9e3779b9L);


    private final static String keyStr = "00000001000000020000000300000004";



    public static long[] decrypt( long[] v,  long[] k)
    {
        int n = v.length;
        long z=v[n-1],y=v[0], sum=0, e;
        int p,q;
        /* 解密过程 */
        q =  6 + 52/n;
        sum =  q*DELTA;
        while(sum >0)
        {
            e =  getUint32((sum >> 2) & 3);
            for (p=n-1; p>0; p--)
            {
                z = v[p-1];
                y = v[p] =  getUint32(v[p]-(getUint32((getUint32(z>>5)^getUint32(y<<2)) + (getUint32(y>>3)^getUint32(z<<4)))^
                        getUint32(getUint32(sum^y) + getUint32(k[(int)(p&3^e)]^z))));
            }
            z = v[n-1];
            y = v[0] = getUint32(v[0]-(getUint32((getUint32(z>>5)^getUint32(y<<2)) + (getUint32(y>>3)^getUint32(z<<4)))^
                    getUint32(getUint32(sum^y) + getUint32(k[(int)(p&3^e)]^z))));
            sum -= DELTA;
        }
        return v;
    }

    public static long[] encrypt(long[] v,  long[] k)
    {
        int n = v.length;
        long z=v[n-1],y=v[0], sum=0, e;
        int p,q;
        /* 加密过程 */
        q = 6 + 52/n;
        while (q-- > 0) {
            sum = getUint32(sum+DELTA);
            e = getUint32((sum >> 2) & 3);
            for (p=0; p<n-1; p++) {
                y = v[p+1];
                z = v[p] = getUint32(v[p]+(getUint32((getUint32(z>>5)^getUint32(y<<2)) + (getUint32(y>>3)^getUint32(z<<4)))^
                        getUint32(getUint32(sum^y) + getUint32(k[(int)(p&3^e)]^z))));
            }
            y = v[0];
            z = v[n-1] = getUint32(v[n-1]+(getUint32((getUint32(z>>5)^getUint32(y<<2)) + (getUint32(y>>3)^getUint32(z<<4)))^
                    getUint32(getUint32(sum^y) + getUint32(k[(int)(p&3^e)]^z))));
        }
        return v;
    }


    public static String encrypt(String source) throws Exception {
        return encrypt(source, keyStr);
    }


    public static String encrypt(String source, String keyStr) throws Exception {
        if (keyStr.length() != 32) {
            throw new Exception("key length must be 32");
        }

        long[] rs = data8todata32(source.toCharArray());
//		char[] zi = new char[16];
//		int[]	ai = new int[16];
//		for(int i = 0;i < keyStr.length();i++) {
//			ai[i] = Integer.parseInt(keyStr.substring(i, i+1));
//			zi[i] = (char) ai[i];
//		}
        long[] key = CrcHelper.separateKey(keyStr);
        encrypt(rs, key);
        char[] output = data32todata8(rs);
        String ciphertext = "";
        for (char o : output) {
            ciphertext += o;//StringUtils.toHexString(o);
        }
        return ciphertext;
    }

    public static String decrypt(String source) throws Exception {
        return decrypt(source, keyStr);
    }

    public static String decrypt(String source, String keyStr) throws Exception {
        if (keyStr.length() != 32) {
            throw new Exception("key length must be 32");
        }
        /**
         * 16进制转字符串
         * */
//		char[] zi = new char[16];
//		int[]	ai = new int[16];
//		for(int i = 0;i < keyStr.length();i++) {
//			ai[i] = Integer.parseInt(keyStr.substring(i, i+1));
//			zi[i] = (char) ai[i];
//		}
        long[] key = CrcHelper.separateKey(keyStr);
        String cs = source;//TeaSupport.hexStringToString(source);
        long[] rs = data8todata32(cs.toCharArray());
        decrypt(rs, key);
        char[] output = data32todata8(rs);
        return String.valueOf(output).trim();
    }


    public static void main(String[] args) throws Exception {


        /**
//         * 包尾网关到中间件 连接和心跳
//         */
//        String cmd = "^(\\$\\$)[.\\n]+?(#@#@01t)$";
//
       String msg = "445478EB912B6BE1E3473726CD9B3FD376FBB34588DE67BCB615F02867C2D15F034E900D8D3CD3A7CC1543B7EC2D71ADA4F26B4B79EF9EADD5D6666394BAD76D6012CE74B375EB3BA0D81E3417DE6FC35DA3C69C9EC52613B5965353";

        String hex = ByteHelper.hexStringToString(msg);

        String s = XTEAHelper.decrypt(hex);

        System.out.println(s);

        //System.out.println(s);
//
//        //System.out.println(encrypt(cmd));
//
//        //System.out.println(bytes);
//
//        if (ValidateHelper.match(cmd, bytes)) {
//            //System.out.println("我成功啦");
//        }




//        try {
//
//            String str = "{\"cmd\":\"connect\",\"crc\":\"9217a642\",\"mark\":\"5751658699368797\",\"pm\":6,\"timestamp\":\"00000020180706114936\",\"token\":\"3030000095\"}";
//            //System.out.println("----------test encryptor-------");
//
//            String m = encrypt(str);
//            //System.out.println(m);
//            //System.out.println(ByteHelper.stringToHexString(m));
//            String rs = decrypt(m);
//            //System.out.println(rs.length());
//            //System.out.println(rs);
//
//
//
//
////            String s2 = "416851BF956B2271F9EAA2C43488A89F628F3D17F415C44509CB95A78194795543411B083CEC047E9BCE856C9A7F3CF72F9C71AFBE1DE9070ABE1CB55D0CB46CE568EE29CCC433F9B4FDEDA5ED2D5AB1D309338E830F";
////
////            //System.out.println(ByteHelper.stringToHexString(s2));
////            String bytes = ByteHelper.hexStringToString(s2);
////
////            //System.out.println(decrypt(bytes));
//
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

    }


}
