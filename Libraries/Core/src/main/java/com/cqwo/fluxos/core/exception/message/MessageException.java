package com.cqwo.fluxos.core.exception.message;

import com.cqwo.fluxos.core.exception.CWMException;
import com.cqwo.fluxos.core.message.MessageInfo;
import lombok.Getter;

/**
 * 消息传递异常
 */
public class MessageException extends CWMException {

    private static final long serialVersionUID = -1066022429566331817L;

    /**
     * 消息
     */
    @Getter
    private MessageInfo messageInfo;

    public MessageException() {
        super();
    }

    public MessageException(String message) {
        super(message);
    }

    public MessageException(MessageInfo messageInfo) {
        super(messageInfo.getMessage());
        this.messageInfo = messageInfo;
    }

    public MessageException(String message, Throwable cause) {
        super(message, cause);
    }

    public MessageException(Throwable cause) {
        super(cause);
    }

    public MessageException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
