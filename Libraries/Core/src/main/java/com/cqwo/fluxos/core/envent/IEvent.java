/*
 *
 *  * Copyright (C) 2017.
 *  * 用于JAVA项目开发
 *  * 重庆英卡电子有限公司 版权所有
 *  * Copyright (C)  2017.  CqingWo Systems Incorporated. All rights reserved.
 *
 */

package com.cqwo.fluxos.core.envent;

/**
 * Created by cqnews on 2017/4/14.
 */

public interface IEvent {

    /**
     * 定时器名称
     *
     * @return 定时器名称
     */
    default String name() {
        return "";
    }

    ;

    /**
     * 执行任务
     */
    void execute();
}
