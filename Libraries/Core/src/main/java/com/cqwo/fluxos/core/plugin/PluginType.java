/*
 *
 *  *
 *  *  * Copyright (C) 2018.
 *  *  * 用于JAVA项目开发
 *  *  * 重庆英卡电子有限公司 版权所有
 *  *  * Copyright (C)  2018.  CqingWo Systems Incorporated. All rights reserved.
 *  *
 *
 */

package com.cqwo.fluxos.core.plugin;

import com.cqwo.fluxos.core.helper.TypeHelper;
import com.cqwo.fluxos.core.model.SelectListItem;
import com.google.common.collect.Lists;
import lombok.Getter;

import java.util.List;

public enum PluginType {

    /**
     * 默认插件
     */
    DefaultPlugin("DefaultPlugin", 0);

    // 成员变量
    @Getter
    private String name;

    @Getter
    private Integer index;

    // 构造方法
    private PluginType(String name, Integer index) {
        this.name = name;
        this.index = index;
    }


    // 普通方法
    public static String getName(Integer index) {
        for (PluginType c : PluginType.values()) {
            if (c.getIndex().equals(index)) {
                return c.name;
            }
        }
        return null;
    }


    /**
     * 获取枚举的列表
     *
     * @return
     */
    public static List<SelectListItem> getSelectListItem() {
        return getSelectListItem(1);
    }

    /**
     * 获取枚举的列表
     * index 默认选择项目
     *
     * @return
     */
    public static List<SelectListItem> getSelectListItem(Integer index) {
        List<SelectListItem> selectListItemList = Lists.newArrayList();

        for (PluginType c : PluginType.values()) {

            SelectListItem item = new SelectListItem();

            item.setText(c.getName());
            item.setValue(TypeHelper.intToString(c.getIndex()));

            if (c.getIndex().equals(index)) {
                item.setSelected(true);
            }

            selectListItemList.add(item);
        }

        return selectListItemList;
    }


    }
