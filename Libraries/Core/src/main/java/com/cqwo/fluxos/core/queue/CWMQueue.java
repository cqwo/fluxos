package com.cqwo.fluxos.core.queue;


import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


/**
 * kafka策略
 */
@Component(value = "CWMKafka")
@Getter
public class CWMQueue {

    @Autowired(required = false)
    private IQueueStrategy iQueueStrategy;

}
