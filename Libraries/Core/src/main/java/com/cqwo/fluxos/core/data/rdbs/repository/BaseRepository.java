package com.cqwo.fluxos.core.data.rdbs.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.NoRepositoryBean;

import java.io.Serializable;


/**
 * 基础
 *
 * @author cqnews
 */
@NoRepositoryBean
public interface BaseRepository<T, ID extends Serializable>

        extends JpaRepository<T, ID>, JpaSpecificationExecutor<T> {


}
