package com.cqwo.fluxos.core.domain.message;


import com.alibaba.fastjson.JSON;
import com.cqwo.fluxos.core.helper.DateHelper;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;


/**
 * 消息模型
 *
 * @author cqnews
 */
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class QueueMessageInfo implements Serializable {


    private static final long serialVersionUID = 5412979067283452183L;
    /**
     * 设备id
     */
    private String deviceSN = "";

    /**
     * 来源
     */
    private String from = "";


    /**
     * token
     */
    private String token = "";


    /**
     * 应用
     */
    private String application = "";


    /**
     * channel
     */
    private String channelId = "";

    /**
     * 报事ip
     */
    private String ip = "";

    /**
     * 端口
     */
    private Integer port = 0;


    /**
     * 消息分类
     */
    private Integer type = 0;


    /**
     * 消息状态
     */
    private Integer state = 0;

    /**
     * 消息说明
     */
    private String message = "";

    /**
     * 有效数据
     */
    private String data = "";

    /**
     * 消息内容
     */
    private Object content = "";

    /**
     * 时间戳
     */
    private Integer timestamp = DateHelper.getUnixTimeStamp();


    /**
     * 转JOSN串
     *
     * @return
     */
    public String toJson() {
        return JSON.toJSONString(this);
    }


}
