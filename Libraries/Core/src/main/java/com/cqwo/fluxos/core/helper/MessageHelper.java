package com.cqwo.fluxos.core.helper;

import com.cqwo.fluxos.core.message.DataMessageInfo;
import org.apache.commons.lang3.StringUtils;

import java.text.MessageFormat;

/**
 * 报文帮助类
 */
public class MessageHelper {


    /**
     * 组装报文
     *
     * @param info 消息
     * @return
     */
    public static String packageMessage(DataMessageInfo info) {

        if (info == null) {
            return "";
        }

        return MessageFormat.format("{0}{1}{2}", info.getHead(), info.getData(), info.getEnd());

    }

    /**
     * 数据解包
     *
     * @param headRegex 包文头格式
     * @param endregex  包文尾格式
     * @param msg       原包
     * @return
     */
    public static DataMessageInfo unpackPageage(String headRegex, String endregex, String msg) {


        String headStr = "";
        String endStr = "";


        if (StringUtils.isNotBlank(headRegex)) {
            headStr = ValidateHelper.findOne(headRegex, msg);
            msg = msg.replace(headStr, "");
        }

        if (StringUtils.isNotBlank(endregex)) {
            endStr = ValidateHelper.findOne(endregex, msg);
            msg = msg.replace(endStr, "");
        }


        return new DataMessageInfo(headStr, msg, endStr);
    }

}
