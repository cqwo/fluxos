import java.util.regex.Pattern;

public class Test2 {


    public static void main(String[] args) {

        //System.out.println();

        Test3 t3 = Test3.exitsType("AA0001000007DF060A102D33C32959AB");

        switch (t3) {

            case A1:
                //System.out.println("我是A1");
                break;
            case A2:
                //System.out.println("我是A2");
                break;
            case A3:
                //System.out.println("我是A3");
                break;
            case A4:
                //System.out.println("我是A4");
                break;
            default:
                //System.out.println("我是default");

        }
    }

}


enum Test3 {

    A1(1, "^(AA)[0-9a-fA-F]{22}C1[0-9a-fA-F]*?(AB)$", "A1"),
    A2(2, "^(AA)[0-9a-fA-F]{22}C2[0-9a-fA-F]*?(AB)$", "A2"),
    A3(3, "^(AA)[0-9a-fA-F]{22}C3[0-9a-fA-F]*?(AB)$", "A3"),
    A4(4, "^(AA)[0-9a-fA-F]{22}C4[0-9a-fA-F]*?(AB)$", "A4");

    Integer index;

    String regex;

    String code;

    Test3(Integer index, String regex, String code) {
        this.index = index;
        this.regex = regex;
        this.code = code;
    }

    public static Test3 exitsType(String s) {

        for (Test3 t : Test3.values()) {

            if (Pattern.matches(t.regex, s)) return t;


        }
        return null;

    }
}