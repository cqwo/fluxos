import com.cqwo.fluxos.core.helper.HexHelper;

public class TempTest {


    public static double countTemp(String hexStr) {

        double value = 0;

        if (hexStr.length() != 4)
            return value;

        String heightHexStr = hexStr.substring(0, 2);
        String lowHexStr = hexStr.substring(2, 4);

        String heightBinaryStr = HexHelper.hexadecimalToBinary(heightHexStr);
        String lowBinaryStr = HexHelper.hexadecimalToBinary(lowHexStr);


        //System.out.println(heightHexStr);
        //System.out.println(lowHexStr);


        if (heightBinaryStr.substring(0, 1).equals("0")) {

            //System.out.println("温度为正");

            value = HexHelper.hexadecimalToDecimalism(hexStr) * 0.1;


        } else {


            String hex15 = heightBinaryStr.substring(1) + lowBinaryStr;

            String reverseStr = HexHelper.binaryReverse(hex15, 15);

            String allHexStr = "0" + reverseStr;

            //System.out.println(allHexStr);

            value = (HexHelper.binaryToDecimalism(allHexStr) + 1) * -0.1;

            //System.out.println("温度为负数");
        }

        return value;
    }

    public static void main(String[] args) {

        double s2 = countTemp("0107");

        //System.out.println(s2);
    }
}
