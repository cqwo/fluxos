import com.cqwo.fluxos.core.helper.HexHelper;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class CRCTest {


    private final static String sregex = "[a-fA-F0-9]{4}";

    /**
     * '
     *
     * @param s
     */
    public static void crc(String s) {


        if (!Pattern.matches(sregex, s)) {
            //System.out.println("不正确的crc校验串");
            return;
        }

        List<String> sslist = splitStr(s, 2); //分隔字符串

        if (sslist.size() < 1) {
            return;
        }


        /**
         * 寄存器的十六位进制数
         */
        String rss = "1111111111111111"; //FFFF

        for (String ss : sslist) {

            String sss = HexHelper.hexadecimalToBinary(ss, 8);


            String rs2 = rss.substring(8);


            String rs3 = HexHelper.binaryExclusiveOr(sss, rs2);


            rss = rss.substring(0, 8) + rs3;


            for (int i = 0; i < 8; i++) {


                String endchar = rss.substring(rss.length() - 1);


                Integer rssInter = HexHelper.binaryToDecimalism(rss);
                rssInter = rssInter >> 1;

                rss = HexHelper.toBinary(rssInter, 16);

                if (endchar.equals("1")) {
                    rss = HexHelper.binaryExclusiveOr(rss, "1010000000000001");
                }
            }

        }


        //System.out.println(rss);


        String[] rss2 = rss.split("");

        int length = rss2.length;

        //System.out.println(length);

        int size = length / 2;

        if (size % 2 == 1) {
            size++;
        }


        for (int i = 0; i < size; i++) {


            String temp = rss2[i];
            rss2[i] = rss2[length - i - 1];
            rss2[length - i - 1] = temp;

        }

        //System.out.println(Arrays.toString(rss2));


        StringBuilder rss3 = new StringBuilder();

        for (String s1 : rss2) {

            rss3.append(s1);
        }

        //System.out.println(rss3);

        //寄存器右移一位
//

        //第一步,把第一个8位进制与16位的crc寄存器的低8位相异或


    }

    public static void main(String[] args) {

        crc("F7CC");

        //System.out.println(splitStr("230F", 2).toString());

    }

    /**
     * 把原始字符串分割成指定长度的字符串列表
     *
     * @param inputString 原始字符串
     * @param length      指定长度
     * @return
     */
    public static List<String> splitStr(String inputString, int length) {

        int size = inputString.length() / length;

        if (inputString.length() % length != 0) {
            size += 1;
        }

        List<String> list = new ArrayList<String>();
        for (int index = 0; index < size; index++) {
            String childStr = substring(inputString, index * length,
                    (index + 1) * length);
            list.add(childStr);
        }
        return list;

    }

    /**
     * 分割字符串，如果开始位置大于字符串长度，返回空
     *
     * @param str 原始字符串
     * @param f   开始位置
     * @param t   结束位置
     * @return
     */
    public static String substring(String str, int f, int t) {
        if (f > str.length())
            return null;
        if (t > str.length()) {
            return str.substring(f, str.length());
        } else {
            return str.substring(f, t);
        }
    }

}
