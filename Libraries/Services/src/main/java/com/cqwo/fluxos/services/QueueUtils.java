package com.cqwo.fluxos.services;


import com.cqwo.fluxos.core.domain.message.QueueMessageInfo;
import com.cqwo.fluxos.core.log.CWMLogs;
import com.cqwo.fluxos.core.queue.CWMQueue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service(value = "QueueUtils")
public class QueueUtils {

    @Autowired
    private CWMQueue cwmQueue;

    @Autowired
    private CWMLogs logs;


    /**
     * kafka消息产生
     *
     * @param exchange exchange
     * @param keys     keys
     * @param msg      消息部分
     */
    @Async
    public void send(String exchange, String keys, QueueMessageInfo msg) {
        send(exchange, keys, msg.toJson());
    }

    /**
     * kafka消息产生
     *
     * @param exchange exchange
     * @param keys     keys
     * @param msg      消息部分
     */
    @Async
    public void send(String exchange, String keys, String msg) {

        try {
            cwmQueue.getIQueueStrategy().send(exchange, keys, msg);
        } catch (Exception ex) {
            logs.write(ex, "kafka消息产生");
        }

    }

    /**
     * 创建队列
     *
     * @param queueName    队列名
     * @param exchangeName 交换机的名字
     */
    public void createQueue(String queueName, String exchangeName) {
        try {
            if (existQueue(queueName, exchangeName)) {
                throw new IllegalArgumentException("队列已经存在");
            }
            cwmQueue.getIQueueStrategy().createQueue(queueName, exchangeName);
        } catch (Exception ex) {
            logs.write(ex, "创建队列");
        }
    }


    /**
     * 创建队列
     *
     * @param queueName    队列名
     * @param exchangeName 交换机的名字
     * @param exchangeName 路由key
     */
    public void createQueue(String queueName, String exchangeName, String routingKey) {
        try {
            if (existQueue(queueName, exchangeName)) {
                throw new IllegalArgumentException("队列已经存在");
            }
            cwmQueue.getIQueueStrategy().createQueue(queueName, exchangeName, routingKey);
        } catch (Exception ex) {
            logs.write(ex, "创建队列");
        }
    }

    /**
     * 判断队列是否存在
     *
     * @param queueName 队列名称
     * @return 队列是否存在
     */
    public boolean existQueue(String queueName, String exchangeName) {
        try {
            return cwmQueue.getIQueueStrategy().existQueue(queueName, exchangeName);
        } catch (Exception ex) {
            logs.write(ex, "判断队列是否存在");
        }
        return false;
    }


}
