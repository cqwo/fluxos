package com.cqwo.fluxos.data;

import com.cqwo.fluxos.core.config.CWMConfig;
import com.cqwo.fluxos.core.sms.CWMSMS;
import com.cqwo.fluxos.core.cache.CWMCache;
import com.cqwo.fluxos.core.data.CWMData;
import org.springframework.beans.factory.annotation.Autowired;


/**
 * data操作部分
 */
public class DataService {

    @Autowired
    protected CWMCache cwmCache;

    @Autowired
    protected CWMData cwmData;

    @Autowired
    protected CWMSMS cwmSMS;

    @Autowired
    protected CWMConfig cwmConfig;

}
